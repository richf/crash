import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableNativeFeedback
} from "react-native";
import { AdMobBanner } from "react-native-admob";
import { StackNavigator } from "react-navigation";

export default class Intro2 extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
        <AdMobBanner
          adSize="smartBannerPortrait"
          adUnitID="ca-app-pub-7500038704754081/6409479386"
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error)}
          style={styles.ads}
        />
        <View style={styles.body}>
          <Image
            source={require("./assets/images/intro02.png")}
            resizeMode={"contain"}
            style={styles.gradientImage}
          />
          <Text style={styles.instructions}>
            When you start driving in an area where there have been numerous
            crashes in the past the screen will transition to an amber warning
            colour.
          </Text>
          <Text style={styles.instructions}>
            When you are in such an area it is advised to drive with caution.
          </Text>
          <Text style={styles.instructions}>
            When you are in a green zone continue to drive safely.
          </Text>
          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("Main")}
            background={TouchableNativeFeedback.SelectableBackground()}
          >
            <View style={styles.button}>
              <Text style={styles.buttonText}>Let's Go!</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#286E93",
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 30
  },
  body: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    paddingTop: 0
  },
  ads: {
    paddingTop: 0
  },
  title: {
    fontSize: 24,
    fontFamily: "SairaExtraCondensed-SemiBold",
    color: "#ffffff",
    textAlign: "center",
    margin: 10
  },
  gradientImage: {
    borderWidth: 1,
    borderColor: "#ffffff",
    width: 200,
    height: 100,
    marginBottom: 20
  },
  instructions: {
    textAlign: "center",
    fontSize: 14,
    fontFamily: "Lato-Regular",
    color: "#ffffff",
    maxWidth: 400,
    marginBottom: 20
  },
  button: {
    paddingLeft: 20,
    paddingRight: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#ffffff",
    marginTop: 20,
    padding: 10,
    backgroundColor: "#0F557A",
    flexDirection: "row"
  },
  buttonText: {
    color: "#ffffff",
    fontSize: 16
  },
  swipe: {
    fontSize: 16,
    fontFamily: "Lato-Regular",
    color: "#ffffff",
    padding: 5
  }
});
