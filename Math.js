// Converts from degrees to radians.
Math.radians = degrees => {
  return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
Math.degrees = radians => {
  return radians * 180 / Math.PI;
};

export const calculateDerivedPosition = (
  point = { lat: 0, lng: 0 },
  range = 100,
  bearing = 0
) => {
  let EarthRadius = 6371000; // m

  let latA = Math.radians(point.lat);
  let lngA = Math.radians(point.lng);
  let angularDistance = range / EarthRadius;
  let trueCourse = Math.radians(bearing);

  let lat = Math.asin(
    Math.sin(latA) * Math.cos(angularDistance) +
      Math.cos(latA) * Math.sin(angularDistance) * Math.cos(trueCourse)
  );

  let dlng = Math.atan2(
    Math.sin(trueCourse) * Math.sin(angularDistance) * Math.cos(latA),
    Math.cos(angularDistance) - Math.sin(latA) * Math.sin(lat)
  );

  let lng = (lngA + dlng + Math.PI) % (Math.PI * 2) - Math.PI;

  lat = Math.degrees(lat);
  lng = Math.degrees(lng);

  let newPoint = { lat: lat, lng: lng };

  return newPoint;
};

export const pointIsInCircle = (pointForCheck, center, radius) => {
  if (getDistanceBetweenTwoPoints(pointForCheck, center) <= radius) return true;
  else return false;
};

export const getDistanceBetweenTwoPoints = (p1, p2) => {
  R = 6371000; // m
  dLat = Math.radians(p2.lat - p1.lat);
  dLng = Math.radians(p2.lng - p1.lng);
  lat1 = Math.radians(p1.lat);
  lat2 = Math.radians(p2.lat);

  a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLng / 2) * Math.sin(dLng / 2) * Math.cos(lat1) * Math.cos(lat2);
  c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  d = R * c;

  return d;
};
