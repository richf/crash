import React, { Component } from "react";
import {
  View,
  ListView,
  StyleSheet,
  Text,
  Animated,
  Image
} from "react-native";
import SQLite from "react-native-sqlite-storage";
import {
  calculateDerivedPosition,
  pointIsInCircle,
  getDistanceBetweenTwoPoints
} from "./Math";
import Address from './Address';

SQLite.enablePromise(true);

let db = null;
const normalizer = 0.025;

export default class Main extends Component<{}> {
  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      progress: [],
      numOfCrashes: null,
      numOfDataRows: null,
      riskPercentage: 0,
      backgroundColor: new Animated.Value(0),
      fadeValue: new Animated.Value(1),
      error: null
    };

    this.getNearByGPSPoints = this.getNearByGPSPoints.bind(this);
    this.loadDB = this.loadDB.bind(this);
    this.deleteDatabase = this.deleteDatabase.bind(this);
    this.closeDatabase = this.closeDatabase.bind(this);
    this.watchLocation = this.watchLocation.bind(this);
    this.getTotalDataLength = this.getTotalDataLength.bind(this);
    this.setBackGroundColour = this.setBackGroundColour.bind(this);
  }

  componentDidMount() {
    this.loadDB();
    this.watchLocation(db);

    Animated.timing(this.state.fadeValue, {
      toValue: 0,
      duration: 3000,
      delay: 60000
    }).start();
  }

  watchLocation() {
    this.watchId = navigator.geolocation.watchPosition(
      position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null
        });
        if (db) {
          db
            .transaction(this.getNearByGPSPoints)
            .then(() => {
              db
                .transaction(this.getTotalDataLength)
                .then(() => {
                  let riskPercentage =
                    this.state.numOfCrashes / this.state.numOfDataRows * 100;
                  this.setState({ riskPercentage: riskPercentage });
                  this.setBackGroundColour();
                })
                .catch(error => this.setState({ error: error.message }));
            })
            .catch(error => this.setState({ error: error.message }));
        }
      },
      error => this.setState({ error: error.message }),
      {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 1000,
        distanceFilter: 10
      }
    );
  }

  setBackGroundColour() {
    let toValue = this.state.riskPercentage / normalizer;
    Animated.timing(this.state.backgroundColor, {
      toValue: toValue,
      duration: 500
    }).start();
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
    this.closeDatabase();
  }

  populateDatabase(db) {
    db
      .executeSql("SELECT * FROM gps_data LIMIT 1;")
      .catch(error => this.setState({ error: error.message }));
  }

  getNearByGPSPoints(tx) {
    let radius = 250;
    let center = { lat: this.state.latitude, lng: this.state.longitude };
    let mult = 1; // mult = 1.1; is more reliable
    let p1 = calculateDerivedPosition(center, mult * radius, 0);
    let p2 = calculateDerivedPosition(center, mult * radius, 90);
    let p3 = calculateDerivedPosition(center, mult * radius, 180);
    let p4 = calculateDerivedPosition(center, mult * radius, 270);

    strWhere =
      " WHERE " +
      "LAT" +
      " > " +
      p3.lat +
      " AND " +
      "LAT" +
      " < " +
      p1.lat +
      " AND " +
      "LNG" +
      " < " +
      p2.lng +
      " AND " +
      "LNG" +
      " > " +
      p4.lng;

    let finalPositions = [];

    tx
      .executeSql("SELECT * FROM gps_data " + strWhere + ";")
      .then(([tx, results]) => {
        var len = results.rows.length;

        let counter = 0;
        for (let i = 0; i < len; i++) {
          let row = results.rows.item(i);
          if (pointIsInCircle({ lat: row.LAT, lng: row.LNG }, center, radius)) {
            counter++;
            finalPositions.push([row.LAT, row.LNG]);
          }
        }
        this.setState({ numOfCrashes: finalPositions.length });
      })
      .catch(error => this.setState({ error: error.message }));
  }

  getTotalDataLength(tx) {
    let totalRows = 0;
    tx
      .executeSql("SELECT count(*) as total FROM gps_data;")
      .then(([tx, results]) => {
        totalRows = results.rows.item(0).total;
        this.setState({ numOfDataRows: totalRows });
      })
      .catch(error => this.setState({ error: error.message }));
  }

  loadDB() {
    SQLite.echoTest()
      .then(() => {
        SQLite.openDatabase({ name: "crashdata", createFromLocation: 1 })
          .then(DB => {
            db = DB;
            this.populateDatabase(DB);
          })
          .catch(error => this.setState({ error: error.message }));
      })
      .catch(error => this.setState({ error: error.message }));
  }

  closeDatabase() {
    if (db) {
      db.close().catch(error => this.setState({ error: error.message }));
    }
  }

  deleteDatabase() {
    SQLite.deleteDatabase("crashdata").catch(error =>
      this.setState({ error: error.message })
    );
  }

  render() {
    let color = this.state.backgroundColor.interpolate({
      inputRange: [0, 1],
      outputRange: ["#46cb34", "#ff5e00"]
    });

    let opacity = this.state.fadeValue;

    return (
      <Animated.View style={[styles.mainContainer, { backgroundColor: color }]}>
        <Animated.View style={{ opacity: opacity }}>
          <Text style={styles.instructions}>
            When you are in a high crash area this screen will change colour.
          </Text>
        </Animated.View>
        <Address latitude={this.state.latitude} longitude={this.state.longitude} />
      </Animated.View>
    );
  }
}

var styles = StyleSheet.create({
  instructions: {
    fontSize: 18,
    fontFamily: "Lato-Regular",
    textAlign: "center",
    color: "#ffffff",
    padding: 10,
    lineHeight: 30
  },
  mainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between"
  }
});
