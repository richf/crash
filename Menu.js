import React from "react";
import PropTypes from "prop-types";
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  Linking
} from "react-native";

const window = Dimensions.get("window");
const logo = require("./assets/images/icon.png");

const styles = StyleSheet.create({
  menu: {
    flex: 1,
    backgroundColor: "#333",
    padding: 20,
    flexDirection: "column",
    justifyContent: "space-around",
    height: window.height
  },
  topContainer: {
    flex: 1
  },
  name: {
    position: "absolute",
    left: 70,
    top: 20
  },
  logoContainer: {
    flex: 1,
    flexDirection: "row"
  },
  logo: {
    width: 25,
    height: 25,
    margin: 10
  },
  title: {
    color: "#ffffff",
    fontSize: 32,
    fontFamily: "SairaExtraCondensed-SemiBold"
  },
  menuTitle: {
    marginTop: 40,
    color: "#ffffff",
    fontSize: 20,
    fontFamily: "SairaExtraCondensed-SemiBold",
    borderTopWidth: 1,
    borderColor: "#ffffff",
    paddingTop: 20
  },
  item: {
    fontSize: 14,
    fontWeight: "300",
    paddingTop: 5,
    color: "#ffffff",
    textDecorationLine: "underline",
    textDecorationStyle: "dashed"
  },
  social: {
    flex: 1,
    flexDirection: "row"
  },
  icon: {
    fontFamily: "fa-brands-400",
    fontSize: 20,
    color: "#ffffff",
    marginRight: 20
  },
  star: {
    fontFamily: "fa-solid-900",
    fontSize: 20,
    color: "#ffffff",
    marginRight: 20
  },
  footer: {
    flexDirection: "row",
    borderTopWidth: 1,
    borderColor: "#ffffff",
    marginTop: 40,
    paddingTop: 20
  }
});

export default function Menu({ onItemSelected }) {
  return (
    <View scrollsToTop={false} style={styles.menu}>
      <View style={styles.topContainer}>
        <Text style={styles.title}>Menu</Text>
        <Text onPress={() => onItemSelected("OnBoarding")} style={styles.item}>
          Intro
        </Text>
        <Text style={styles.menuTitle}>Keep in Touch</Text>
        <View style={styles.social}>
          <Text
            style={styles.icon}
            onPress={() => Linking.openURL("https://twitter.com/CrashZoneApp")}
          >
            &#xf081;
          </Text>
          <Text
            style={styles.icon}
            onPress={() =>
              Linking.openURL("https://www.instagram.com/crashzoneapp/")
            }
          >
            &#xf16d;
          </Text>
        </View>
        <Text style={styles.menuTitle}>Rate this app</Text>
          <Text
            style={styles.star}
            onPress={() => Linking.openURL("market://details?id=com.nzcrashzone")}
          >
            &#xf005; &#xf005; &#xf005; &#xf005; &#xf005;
          </Text>
      </View>
      <View style={styles.footer}>
        <Text style={styles.title}>NZ Crash Zone</Text>
        <Image source={logo} style={styles.logo} />
      </View>
    </View>
  );
}

Menu.propTypes = {
  onItemSelected: PropTypes.func.isRequired
};
