import React, { Component } from "react";
import PropTypes from "prop-types";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";

const image = require("./assets/images/menu.png");
const logo = require("./assets/images/icon.png");

export default class Navbar extends Component<{}> {
  render() {
    return (
      <View style={styles.navbar}>
        <View style={styles.container}>
          <TouchableOpacity onPress={this.props.toggle} style={styles.button}>
            <Image source={image} style={styles.hamburger} />
          </TouchableOpacity>

          <Text style={styles.navbarTitle}>NZ Crash Zone</Text>
          <Image
            source={logo}
            resizeMode={"contain"}
            style={styles.navbarLogo}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  navbar: {
    backgroundColor: "#286E93",
    alignSelf: "stretch",
    height: 60,
    padding: 0
  },
  hamburger: {
    width: 32,
    height: 32,
    marginTop: 4
  },
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 10
  },
  navbarLogo: {
    width: 40,
    height: 40
  },
  navbarTitle: {
    fontSize: 32,
    fontFamily: "SairaExtraCondensed-SemiBold",
    color: "#ffffff"
  }
});

Navbar.propTypes = {
  toggle: PropTypes.func.isRequired
};
