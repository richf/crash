import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableNativeFeedback
} from "react-native";
import PropTypes from "prop-types";
import { AdMobBanner } from "react-native-admob";
import { StackNavigator } from "react-navigation";

export default class Intro1 extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
        <AdMobBanner
          adSize="smartBannerPortrait"
          adUnitID="ca-app-pub-7500038704754081/6409479386"
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error)}
          style={styles.ads}
        />
        <View style={styles.body}>
          <Image
            source={require("./assets/images/intro01.png")}
            resizeMode={"contain"}
            style={styles.introImage}
          />
          <Text style={styles.instructions}>
            First dock your phone in your hands free car phone mount.
          </Text>

          <TouchableNativeFeedback
            onPress={() => this.props.scrollBy(1)}
            background={TouchableNativeFeedback.SelectableBackground()}
          >
            <View style={styles.button}>
              <Text style={styles.buttonText}>Next</Text>
              <Text style={styles.icon}>&#xf054;</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#286E93",
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 30
  },
  body: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    paddingTop: 0
  },
  ads: {
    paddingTop: 0
  },
  introImage: {
    height: 150,
    marginBottom: 20
  },
  title: {
    fontSize: 24,
    fontFamily: "SairaExtraCondensed-SemiBold",
    color: "#ffffff",
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    fontSize: 14,
    fontFamily: "Lato-Regular",
    color: "#ffffff",
    maxWidth: 400,
    marginBottom: 20
  },
  button: {
    paddingLeft: 20,
    paddingRight: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#ffffff",
    marginTop: 20,
    padding: 10,
    backgroundColor: "#0F557A",
    flexDirection: "row"
  },
  buttonText: {
    color: "#ffffff",
    fontSize: 16
  },
  icon: {
    fontFamily: "fa-solid-900",
    fontSize: 20,
    color: "#ffffff",
    marginLeft: 20
  },
  swipe: {
    fontSize: 16,
    fontFamily: "Lato-Regular",
    color: "#ffffff",
    padding: 5
  }
});

Intro1.propTypes = {
  scrollBy: PropTypes.func.isRequired
};
