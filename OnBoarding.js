import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Swiper from "react-native-swiper";
import Intro1 from "./Intro1";
import Intro2 from "./Intro2";
import Splash from "./Splash";

export default class OnBoarding extends Component<{}> {
  scrollTo = index => {
    this.swiper.scrollBy(index)
  }

  render() {
    return (
      <Swiper
        ref={(swiper) => {this.swiper = swiper;}}
        style={styles.wrapper}
        showsButtons={false}
        dot={<View style={styles.dot} />}
        activeDot={<View style={styles.activeDot} />}
      >
        <Splash
          tabLabel="Welcome"
          navigation={this.props.navigation}
          scrollBy={this.scrollTo}
        />
        <Intro1 tabLabel="Intro" scrollBy={this.scrollTo}/>
        <Intro2 tabLabel="Start" navigation={this.props.navigation} />
      </Swiper>
    );
  }
}

var styles = StyleSheet.create({
  dot: {
    backgroundColor: "rgba(0,0,0,.15)",
    width: 6,
    height: 6,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3
  },
  activeDot: {
    backgroundColor: "rgba(0,0,0,0)",
    borderWidth: 1,
    borderColor: "rgba(255,255,255,0.8)",
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  }
});
