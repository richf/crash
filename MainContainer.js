import React, { Component } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import SideMenu from "react-native-side-menu";
import Menu from "./Menu";
import Navbar from "./Navbar";
import KeepAwake from "react-native-keep-awake";
import Main from "./Main";

export default class MainContainer extends Component<{}> {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);

    this.state = {
      isOpen: false,
      selectedItem: "About"
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  updateMenuState(isOpen) {
    this.setState({ isOpen });
  }

  onMenuItemSelected = item => {
    this.setState({
      isOpen: false,
      selectedItem: item
    });
    this.props.navigation.navigate(item)
  }


  render() {
    const menu = <Menu onItemSelected={this.onMenuItemSelected} />;

    return (
      <SideMenu
        menu={menu}
        isOpen={this.state.isOpen}
        onChange={isOpen => this.updateMenuState(isOpen)}
      >
        <Navbar
          toggle={this.toggle}/>
        <KeepAwake />
        <Main />
      </SideMenu>
    );
  }
}

const styles = StyleSheet.create({

});
