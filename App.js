/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import { StackNavigator } from "react-navigation";
import Swiper from "react-native-swiper";

import MainContainer from "./MainContainer";
import OnBoarding from "./OnBoarding";

export default class App extends Component<{}> {
  render() {
    return <RootStack />;
  }
}

const RootStack = StackNavigator(
  {
    OnBoarding: {
      screen: OnBoarding
    },
    Main: {
      screen: MainContainer
    }
  },
  {
    initialRouteName: "OnBoarding",
    headerMode: "none",
  }
);
