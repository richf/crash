import React, { Component } from "react";
import { StyleSheet, Text } from "react-native";
import throttle from 'lodash.throttle';

const Nominatim = require("nominatim-geocoder");
const geocoder = new Nominatim({
  delay: 1000
});

export default class Address extends Component<{}> {
  constructor(props) {
    super(props);

    this.state = {
      streetName: null
    };

    this.getStreetName = this.getStreetName.bind(this);
    this.getStreetNameThrottled = throttle(this.getStreetName, 3000);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.latitude != this.props.latitude ||
      prevProps.longitude != this.props.longitude
    ) {
      this.getStreetNameThrottled();
    }
  }

  componentWillUnmount() {
    this.getStreetNameThrottled.cancel();
  }

  getStreetName() {
    geocoder
      .reverse({
        lat: this.props.latitude,
        lon: this.props.longitude,
        format: "json"
      })
      .then(response => {
        this.setState({
          streetName: response.address.road
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <Text style={styles.streetName}>
        {this.state.streetName ? this.state.streetName : ""}
      </Text>
    );
  }
}

var styles = StyleSheet.create({
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  },
  streetName: {
    color: "#ffffff",
    fontFamily: "Lato-Regular",
    fontSize: 24,
    paddingBottom: 20
  },
});
