import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableNativeFeedback,
  Image
} from "react-native";
import PropTypes from "prop-types";
import { AdMobBanner } from "react-native-admob";

export default class Splash extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
        <AdMobBanner
          adSize="smartBannerPortrait"
          adUnitID="ca-app-pub-7500038704754081/6409479386"
          testDevices={[AdMobBanner.simulatorId]}
          onAdFailedToLoad={error => console.error(error)}
          style={styles.ads}
        />
        <View style={styles.body}>
          <Image
            source={require("./assets/images/icon.png")}
            resizeMode={"contain"}
            style={{ width: 100, height: 100 }}
          />
          <Text style={styles.title}>NZ Crash Zone</Text>
          <Text style={styles.instructions}>
            A distraction free app to keep you aware of historically high risk
            crash areas.
          </Text>
          <TouchableNativeFeedback
            onPress={() => this.props.scrollBy(1)}
            background={TouchableNativeFeedback.SelectableBackground()}
          >
            <View style={styles.button}>
              <Text style={styles.buttonText}>Introduction</Text>
              <Text style={styles.icon}>&#xf054;</Text>
            </View>
          </TouchableNativeFeedback>
          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("Main")}
            background={TouchableNativeFeedback.SelectableBackground()}
          >
            <View style={styles.button}>
              <Text style={styles.buttonText}>Let's Go!</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#286E93",
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 30
  },
  body: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    paddingTop: 20
  },
  ads: {
    paddingTop: 0
  },
  title: {
    fontSize: 24,
    fontFamily: "SairaExtraCondensed-SemiBold",
    color: "#ffffff",
    textAlign: "center",
    margin: 10
  },
  button: {
    paddingLeft: 20,
    paddingRight: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: "#ffffff",
    marginTop: 40,
    padding: 10,
    backgroundColor: "#0F557A",
    flexDirection: "row"
  },
  buttonText: {
    color: "#ffffff",
    fontSize: 16
  },
  icon: {
    fontFamily: "fa-solid-900",
    fontSize: 20,
    color: "#ffffff",
    marginLeft: 20
  },
  instructions: {
    textAlign: "center",
    fontFamily: "Lato-Regular",
    color: "#ffffff",
    maxWidth: 400
  },
  swipe: {
    fontSize: 16,
    fontFamily: "Lato-Regular",
    color: "#ffffff",
    padding: 5
  }
});

Splash.propTypes = {
  scrollBy: PropTypes.func.isRequired
};
